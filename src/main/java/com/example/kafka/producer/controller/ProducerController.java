package com.example.kafka.producer.controller;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Arkady on 19.04.2020
 */

@RestController
@RequestMapping("message")
public class ProducerController {

    private final static String TOPIC_1 = "test-kafka-topic-1";
    private final static String TOPIC_2 = "test-kafka-topic-2";

    private final static int PARTITION_0 = 0;
    private final static int PARTITION_1 = 1;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @PostMapping
    public void msgSender() {
        final String message = "Hello, Kafka!";
        System.out.println("Send msg: " + message);
        kafkaTemplate.send(TOPIC_1, message);
    }

    @PostMapping("/topic_1/partition_1")
    public void msgSender1(String id, String msg) {
        ListenableFuture<SendResult<String, String>> future;
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                ProducerRecord<String, String> recordToPartition0 = new ProducerRecord<>(TOPIC_2, PARTITION_0, id + "_" + i, msg + "_" + i);
                future = kafkaTemplate.send(recordToPartition0);
            } else {
                ProducerRecord<String, String> recordToPartition1 = new ProducerRecord<>(TOPIC_2, PARTITION_1, id + "_" + i, msg + "_" + i);
                future = kafkaTemplate.send(recordToPartition1);
            }

            future.addCallback(System.out::println, System.err::println);
            kafkaTemplate.flush();
        }
    }

    @PostMapping("/topic_1/partition_2")
    public void msgSender2(String id, String msg) {
        for (int i = 10; i < 15; i++) {
            ProducerRecord<String, String> recordToPartition1 = new ProducerRecord<>(TOPIC_2, PARTITION_1, id + "_" + i, msg + "_" + i);
            ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(recordToPartition1);

            future.addCallback(System.out::println, System.err::println);
            kafkaTemplate.flush();
        }
    }
}
